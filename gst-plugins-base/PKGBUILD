pkgbase=gst-plugins-base
pkgname=('gst-plugins-base-libs' 'gst-plugins-base')
pkgver=1.10.3
pkgrel=1
pkgdesc="GStreamer Multimedia Framework Base Plugins"
arch=('x86_64')
license=('LGPL')
makedepends=('pkgconfig' 'gstreamer' 'orc' 'libxv' 'alsa-lib' 'cdparanoia' 'libvisual' 'libvorbis' 'libtheora' 'pango' 'gobject-introspection' 'python2')
options=(!emptydirs)
url="http://gstreamer.freedesktop.org/"
source=(${url}/src/$pkgbase/$pkgbase-${pkgver}.tar.xz{,.asc})
sha256sums=('e6299617d705a0cbfb535107c1d3a8fc0f0967f14193a8c5c7583f46a88b1710'
            'SKIP')
validpgpkeys=('7F4BC7CC3CA06F97336BBFEB0668CC1486C2D7B5')

build() {
  cd $pkgbase-$pkgver

  ./configure --prefix=/usr --sysconfdir=/etc --localstatedir=/var \
    --disable-static --enable-experimental \
    --with-package-name="GStreamer Base Plugins (Chakra GNU/Linux)" \
    --with-package-origin="https://chakralinux.org/"

  # https://bugzilla.gnome.org/show_bug.cgi?id=655517
  sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool

  make

  sed -e 's/^SUBDIRS_EXT =.*/SUBDIRS_EXT =/' -i Makefile
}

check() {
  cd $pkgbase-$pkgver
  # Testsuite fails on one test. Some refcount leak
  make check
}

package_gst-plugins-base-libs() {
  pkgdesc="GStreamer Multimedia Framework Base Plugin libraries"
  depends=('gstreamer' 'orc' 'libxv')

  cd $pkgbase-$pkgver
  make DESTDIR="${pkgdir}" install
}

package_gst-plugins-base() {
  depends=("gst-plugins-base-libs=$pkgver" 'alsa-lib' 'cdparanoia' 'libvisual' 'libvorbis' 'libtheora' 'pango')

  cd $pkgbase-$pkgver
  make -C gst-libs DESTDIR="${pkgdir}" install
  make -C ext DESTDIR="${pkgdir}" install
  make -C gst-libs DESTDIR="${pkgdir}" uninstall
}
