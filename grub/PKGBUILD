_UNIFONT_VER="10.0.06"

_EFI_ARCH="x86_64"

pkgname="grub"
pkgdesc="GNU GRand Unified Bootloader (2)"
pkgver=2.02
pkgrel=2
url="https://www.gnu.org/software/grub/"
arch=('x86_64')
license=('GPL3')
backup=('boot/grub/grub.cfg' 'etc/default/grub' 'etc/grub.d/40_custom')
install="${pkgname}.install"
options=('!makeflags')

conflicts=('grub2-common' 'grub2-bios' 'grub2-efi64' 'grub2-efi32' "grub2-efi-${_EFI_ARCH}")
replaces=('grub2-common' 'grub2-bios' 'grub2-efi64' 'grub2-efi32' "grub2-efi-${_EFI_ARCH}")
provides=('grub2-common' 'grub2-bios' 'grub2-efi64' 'grub2-efi32' "grub2-efi-${_EFI_ARCH}")

makedepends=('git' 'rsync' 'xz' 'freetype2' 'ttf-dejavu' 'python3' 'autogen'
             'texinfo' 'help2man' 'gettext' 'device-mapper' 'fuse')
depends=('sh' 'xz' 'gettext' 'device-mapper')
optdepends=('freetype2: For grub-mkfont usage'
            'fuse: For grub-mount usage'
            'dosfstools: For grub-mkrescue FAT FS and EFI support' 
            'efibootmgr: For grub-install EFI support'
            'libisoburn: Provides xorriso for generating grub rescue iso using grub-mkrescue'
            'os-prober: To detect other OSes when generating grub.cfg in BIOS systems'
            'mtools: For grub-mkrescue FAT FS support')

source=("grub-$pkgver::git+git://git.sv.gnu.org/grub.git#tag=grub-$pkgver"
        "grub-extras::git+git://git.sv.gnu.org/grub-extras.git#commit=f2a079441939eee7251bf141986cdd78946e1d20"
        "https://ftp.gnu.org/gnu/unifont/unifont-${_UNIFONT_VER}/unifont-${_UNIFONT_VER}.bdf.gz"{,.sig}
        '0003-10_linux-detect-archlinux-initramfs.patch'
        '0004-add-GRUB_COLOR_variables.patch'
        '0005-Allow_GRUB_to_mount_ext234_filesystems_that_have_the_encryption_feature.patch'
        'grub.default'
        'update-grub')

md5sums=('SKIP'
         'SKIP'
         '14d3e4d86b7134e39224848e582496ed'
         'SKIP'
         'ba9d27c44b677bf329e5b96933bdbde8'
         'e506ae4a9f9f7d1b765febfa84e10d48'
         '7d4aa743cfabe49158e970a97f7795f0'
         '31e4c7414cef3db3fb26c40ac8da97f2'
         '9910bdfd42fa7d3e21e6961bfcc977f9')
validpgpkeys=('95D2E9AB8740D8046387FD151A09227B1F435A33') # Paul Hardy <unifoundry@unifoundry.com>

prepare() {
	
	cd "${srcdir}/grub-${pkgver}/"
	
	msg "Patch to detect of Chakra Linux initramfs images by grub-mkconfig (same as Arch)"
	patch -Np1 -i "${srcdir}/0003-10_linux-detect-archlinux-initramfs.patch"
	
	msg "Patch to enable GRUB_COLOR_* variables in grub-mkconfig"
	## Based on http://lists.gnu.org/archive/html/grub-devel/2012-02/msg00021.html
	patch -Np1 -i "${srcdir}/0004-add-GRUB_COLOR_variables.patch"

        msg "Patch to allow GRUB to mount ext2/3/4 filesystems that have the encryption feature"
	patch -Np1 -i "${srcdir}/0005-Allow_GRUB_to_mount_ext234_filesystems_that_have_the_encryption_feature.patch"
	
	msg "Fix DejaVuSans.ttf location so that grub-mkfont can create *.pf2 files for starfield theme"
	sed 's|/usr/share/fonts/dejavu|/usr/share/fonts/dejavu /usr/share/fonts/TTF|g' -i "${srcdir}/grub-${pkgver}/configure.ac"
	
	msg "Fix mkinitcpio 'rw' FS#36275"
	sed 's| ro | rw |g' -i "${srcdir}/grub-${pkgver}/util/grub.d/10_linux.in"
	
	msg "Fix OS naming FS#33393"
	sed 's|GNU/Linux|Linux|' -i "${srcdir}/grub-${pkgver}/util/grub.d/10_linux.in"
	
	msg "autogen.sh requires python (2/3). since bzr is in makedepends"
	sed 's|python|python3|g' -i "${srcdir}/grub-${pkgver}/autogen.sh"
	
	msg "Pull in latest language files"
	./linguas.sh
	
	msg "Remove not working langs which need LC_ALL=C.UTF-8"
	sed -e 's#en@cyrillic en@greek##g' -i "${srcdir}/grub-${pkgver}/po/LINGUAS"
	
	msg "Avoid problem with unifont during compile of grub, http://savannah.gnu.org/bugs/?40330 and https://bugs.archlinux.org/task/37847"
	cp "${srcdir}/unifont-${_UNIFONT_VER}.bdf" "${srcdir}/grub-${pkgver}/unifont.bdf"	
}

_build_grub-common_and_bios() {
	
	msg "Copy the source for building the bios part"
	cp -r "${srcdir}/grub-${pkgver}" "${srcdir}/grub-${pkgver}-bios"
	cd "${srcdir}/grub-${pkgver}-bios/"
	
	msg "Add the grub-extra sources for bios build"
	install -d "${srcdir}/grub-${pkgver}-bios/grub-extras"
	cp -r "${srcdir}/grub-extras/915resolution" "${srcdir}/grub-${pkgver}-bios/grub-extras/915resolution"
	export GRUB_CONTRIB="${srcdir}/grub-${pkgver}-bios/grub-extras/"
	
	msg "Unset all compiler FLAGS for bios build"
	unset CFLAGS
	unset CPPFLAGS
	unset CXXFLAGS
	unset LDFLAGS
	unset MAKEFLAGS
	
	cd "${srcdir}/grub-${pkgver}-bios/"
	
	msg "Run autogen.sh for bios build"
	./autogen.sh
	
	msg "Run ./configure for bios build"
	./configure \
		--with-platform="pc" \
		--target="i386" \
		--enable-efiemu \
		--enable-mm-debug \
		--enable-nls \
		--enable-device-mapper \
		--enable-cache-stats \
		--enable-boot-time \
		--enable-grub-mkfont \
		--enable-grub-mount \
		--prefix="/usr" \
		--bindir="/usr/bin" \
		--sbindir="/usr/sbin" \
		--mandir="/usr/share/man" \
		--infodir="/usr/share/info" \
		--datarootdir="/usr/share" \
		--sysconfdir="/etc" \
		--program-prefix="" \
		--with-bootdir="/boot" \
		--with-grubdir="grub" \
		--disable-silent-rules \
		--disable-werror
	
	msg "Run make for bios build"
	make
}

_build_grub-efi() {
	
	msg "Copy the source for building the ${_EFI_ARCH} efi part"
	cp -r "${srcdir}/grub-${pkgver}" "${srcdir}/grub-${pkgver}-efi-${_EFI_ARCH}"
	cd "${srcdir}/grub-${pkgver}-efi-${_EFI_ARCH}/"
	
	msg "Unset all compiler FLAGS for ${_EFI_ARCH} efi build"
	unset CFLAGS
	unset CPPFLAGS
	unset CXXFLAGS
	unset LDFLAGS
	unset MAKEFLAGS
	
	cd "${srcdir}/grub-${pkgver}-efi-${_EFI_ARCH}/"
	
	msg "Run autogen.sh for ${_EFI_ARCH} efi build"
	./autogen.sh
	echo
	
	msg "Run ./configure for ${_EFI_ARCH} efi build"
	./configure \
		--with-platform="efi" \
		--target="${_EFI_ARCH}" \
		--disable-efiemu \
		--enable-mm-debug \
		--enable-nls \
		--enable-device-mapper \
		--enable-cache-stats \
		--enable-boot-time \
		--enable-grub-mkfont \
		--enable-grub-mount \
		--prefix="/usr" \
		--bindir="/usr/bin" \
		--sbindir="/usr/sbin" \
		--mandir="/usr/share/man" \
		--infodir="/usr/share/info" \
		--datarootdir="/usr/share" \
		--sysconfdir="/etc" \
		--program-prefix="" \
		--with-bootdir="/boot" \
		--with-grubdir="grub" \
		--disable-silent-rules \
		--disable-werror
	
	msg "Run make for ${_EFI_ARCH} efi build"
	make
}

build() {
	
	cd "${srcdir}/grub-${pkgver}/"
	
	msg 'Build grub bios stuff'
	_build_grub-common_and_bios
	
	msg "Build grub ${_EFI_ARCH} efi stuff"
	_build_grub-efi

	msg 'Build grub i386 efi stuff'
	_EFI_ARCH='i386' _build_grub-efi
}

_package_grub-common_and_bios() {
	
	cd "${srcdir}/grub-${pkgver}-bios/"
	
	msg "Run make install for bios build"
	make DESTDIR="${pkgdir}/" bashcompletiondir="/usr/share/bash-completion/completions" install
	
	msg "Remove gdb debugging related files for bios build"
	rm -f "${pkgdir}/usr/lib/grub/i386-pc"/*.module
	rm -f "${pkgdir}/usr/lib/grub/i386-pc"/*.image
	rm -f "${pkgdir}/usr/lib/grub/i386-pc"/{kernel.exec,gdb_grub,gmodule.pl}
	
	msg "Install /etc/default/grub (used by grub-mkconfig)"
	install -D -m0644 "${srcdir}/grub.default" "${pkgdir}/etc/default/grub"
	
	msg "Install update-grub (Chakra helper script)"
        install -D -m755 "${srcdir}/update-grub" "${pkgdir}/usr/sbin/update-grub"
}

_package_grub-efi() {
	
	cd "${srcdir}/grub-${pkgver}-efi-${_EFI_ARCH}/"
	
	msg "Run make install for ${_EFI_ARCH} efi build"
	make DESTDIR="${pkgdir}/" bashcompletiondir="/usr/share/bash-completion/completions" install
	
	msg "Remove gdb debugging related files for ${_EFI_ARCH} efi build"
	rm -f "${pkgdir}/usr/lib/grub/${_EFI_ARCH}-efi"/*.module
	rm -f "${pkgdir}/usr/lib/grub/${_EFI_ARCH}-efi"/*.image
	rm -f "${pkgdir}/usr/lib/grub/${_EFI_ARCH}-efi"/{kernel.exec,gdb_grub,gmodule.pl}
}

package() {	
	cd "${srcdir}/grub-${pkgver}/"
	
	msg "Package grub ${_EFI_ARCH} efi stuff"
	_package_grub-efi

	msg 'Package grub i386 efi stuff'
	_EFI_ARCH="i386" _package_grub-efi
	
	msg 'Package grub bios stuff'
	_package_grub-common_and_bios
}
