# maintainer Manuel Tortosa

# Note: you have to rebuild those pkgs
# Core: repo-clean
# Platform: akonadi (exempi uses boost just as makedep)
# Desktop: kdeedu (kdenetwork, kdepimlibs, kdepim, kdesdk and koffice using boost just as makedep)
 
pkgbase=boost
pkgname=('boost-libs' 'boost')
pkgver=1.65.0
_boostver=${pkgver//./_}
pkgrel=1
url='http://www.boost.org/'
arch=('x86_64')
license=('custom')
makedepends=('icu' 'python' 'python2' 'bzip2' 'zlib' 'openmpi')
source=(https://downloads.sourceforge.net/project/${pkgbase}/${pkgbase}/${pkgver}/${pkgbase}_${_boostver}.tar.bz2)
sha256sums=('ea26712742e2fb079c2a566a31f3266973b76e38222b9f88b387e3c8b2f9902c')

build() {
   export _stagedir="${srcdir}/stagedir"
   local JOBS="$(sed -e 's/.*\(-j *[0-9]\+\).*/\1/' <<< ${MAKEFLAGS})"

   cd ${pkgbase}_${_boostver}

   ./bootstrap.sh --with-toolset=gcc --with-icu --with-python=/usr/bin/python2

   _bindir="bin.linuxx86_64"
   install -Dm755 tools/build/src/engine/${_bindir}/b2 "${_stagedir}"/bin/b2

   # Support for OpenMPI
   echo "using mpi ;" >> project-config.jam
   # Add an extra python version. This does not replace anything and python 2.x
   # need to be the default.
   echo "using python : 3.6 : /usr/bin/python3 : /usr/include/python3.6m : /usr/lib ;" \
      >> project-config.jam

   # boostbook is needed by quickbook
   install -dm755 "${_stagedir}"/share/boostbook
   cp -a tools/boostbook/{xsl,dtd} "${_stagedir}"/share/boostbook/

   # default "minimal" install: "release link=shared,static
   # runtime-link=shared threading=single,multi"
   # --layout=tagged will add the "-mt" suffix for multithreaded libraries
   # and installs includes in /usr/include/boost.
   # --layout=system no longer adds the -mt suffix for multi-threaded libs.
   # install to ${_stagedir} in preparation for split packaging
   "${_stagedir}"/bin/b2 \
      variant=release \
      debug-symbols=off \
      threading=multi \
      runtime-link=shared \
      link=shared,static \
      toolset=gcc \
      python=2.7 \
      cflags="${CPPFLAGS} ${CFLAGS} -fPIC -O3" \
      cxxflags="${CPPFLAGS} ${CXXFLAGS} -std=c++14 -fPIC -O3" \
      linkflags="${LDFLAGS}" \
      --layout=system \
      ${JOBS} \
      \
      --prefix="${_stagedir}" \
      install

   # because b2 in boost 1.62.0 doesn't seem to respect python parameter, we
   # need another run for liboost_python3.so
   sed -e '/using python/ s@;@: /usr/include/python${PYTHON_VERSION/3*/${PYTHON_VERSION}m} ;@' \
      -i bootstrap.sh

   ./bootstrap.sh --with-toolset=gcc --with-icu --with-python=/usr/bin/python3 \
      --with-libraries=python

   "${_stagedir}"/bin/b2 clean
   "${_stagedir}"/bin/b2 \
      variant=release \
      debug-symbols=off \
      threading=multi \
      runtime-link=shared \
      link=shared,static \
      toolset=gcc \
      python=3.6 \
      cflags="${CPPFLAGS} ${CFLAGS} -fPIC -O3" \
      cxxflags="${CPPFLAGS} ${CXXFLAGS} -std=c++14 -fPIC -O3" \
      linkflags="${LDFLAGS}" \
      --layout=system \
      ${JOBS} \
      \
      --prefix="${_stagedir}/python3" \
      --with-python \
      install
}

package_boost() {
   pkgdesc='Free peer-reviewed portable C++ source libraries - development headers'
   depends=("boost-libs=${pkgver}")
   optdepends=('python2: for python2 bindings'
               'python3: for python3 bindings'
               'boost-build: to use boost jam for building your project.')
   options=('staticlibs')

   install -dm755 "${pkgdir}"/usr
   cp -a "${_stagedir}"/{bin,include,share} "${pkgdir}"/usr

   install -d "${pkgdir}"/usr/lib
   cp -a "${_stagedir}"/lib/*.a "${pkgdir}"/usr/lib/

   install -Dm644 "${srcdir}/"${pkgbase}_${_boostver}/LICENSE_1_0.txt \
      "${pkgdir}"/usr/share/licenses/boost/LICENSE_1_0.txt

   install -Dm644 "${_stagedir}"/python3/lib/libboost_python3.a \
      "${pkgdir}"/usr/lib/libboost_python3.a

   ln -s /usr/bin/b2 "$pkgdir"/usr/bin/bjam
}

package_boost-libs() {
   pkgdesc='Free peer-reviewed portable C++ source libraries - runtime libraries'
   depends=('bzip2' 'zlib' 'icu')
   optdepends=('openmpi: for mpi support')

   # powerdns-recursor keeps being rebuild against outdated boost-libs
   provides=('libboost_context.so')

   install -dm755 "${pkgdir}"/usr
   cp -a "${_stagedir}"/lib "${pkgdir}"/usr
   cp -a "${_stagedir}"/python3/lib/libboost_python3* "${pkgdir}"/usr/lib
   rm "${pkgdir}"/usr/lib/*.a

   install -Dm644 "${srcdir}/"${pkgbase}_${_boostver}/LICENSE_1_0.txt \
      "${pkgdir}"/usr/share/licenses/boost-libs/LICENSE_1_0.txt
}
