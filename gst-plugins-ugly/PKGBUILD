# Maintainer: Francesco Marinucci < franzmari[at]chakra-project[dot]it>
# contributor Jan de Groot <jgc@archlinux.org>

pkgname=gst-plugins-ugly
pkgver=1.10.3
pkgrel=1
pkgdesc="GStreamer Multimedia Framework Ugly Plugins"
arch=('x86_64')
license=('LGPL')
url="http://gstreamer.freedesktop.org/"
depends=('gst-plugins-base-libs' 'libdvdread' 'lame' 'libmpeg2' 'a52dec' 'libmad' 'libsidplay' 'libcdio' 'libx264' 'opencore-amr')
makedepends=('python' 'x264' 'valgrind')
options=(!emptydirs)
source=(${url}/src/$pkgname/$pkgname-$pkgver.tar.xz{,.asc})
sha256sums=('c91597d03abff9df435ad4892eae44df1ee14159c7cc7317ac9d2766ff446bd2'
            'SKIP')
validpgpkeys=('7F4BC7CC3CA06F97336BBFEB0668CC1486C2D7B5')

build() {
  cd $pkgname-$pkgver

  ./configure --prefix=/usr \
    --sysconfdir=/etc \
    --localstatedir=/var \
    --disable-static \
    --enable-experimental \
    --with-package-name="GStreamer Ugly Plugins (Chakra GNU/Linux)" \
    --with-package-origin="https://chakralinux.org/"
  
  # https://bugzilla.gnome.org/show_bug.cgi?id=655517
  sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool

  make
}

check() {
  cd $pkgname-$pkgver
  make check
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="${pkgdir}" install
}
