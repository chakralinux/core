
pkgname=pacman
pkgver=5.0.1
pkgrel=1
pkgdesc="A library-based package manager with dependency support"
arch=('x86_64')
url="http://www.archlinux.org/pacman/"
license=('GPL')
groups=('base' 'base-devel')
depends=('bash>=4.3.025-1' 'glibc>=2.20-2' 'libarchive=3.2.1' 'curl>=7.28.1'
         'gpgme' 'pacman-mirrorlist' 'nettle>=3.1')
checkdepends=('python2' 'fakechroot')
makedepends=('asciidoc')
optdepends=('fakeroot: for makepkg usage as normal user')
provides=('pacman-contrib')
conflicts=('pacman-contrib')
replaces=('pacman-contrib')
backup=(etc/pacman.conf etc/makepkg.conf)
install=pacman.install
options=(!libtool)
source=(https://sources.archlinux.org/other/pacman/$pkgname-$pkgver.tar.gz{,.sig}
        pacman-5.0.1-roundup.patch::'https://git.archlinux.org/svntogit/packages.git/plain/trunk/pacman-5.0.1-roundup.patch?h=packages/pacman'
        pacman.conf
        makepkg.conf)
sha1sums=('d0157ab851d570d640e253527b336bcfad6cb13c'
          'SKIP'
          '34b85e8040ff483f3814c27eb838bc4d898bbfbf'
          '735dfdbf78f1249567b3250a2766f55eb6ae9a0c'
          '0e61e106d58a332ac7bb0a34885358d37297886e')
validpgpkeys=('6645B0A8C7005E78DB1D7864F99FFE0FEAE999BD')  # Allan McRae <allan@archlinux.org>

prepare() {
   cd "$pkgname-$pkgver"
   # v5.0.1..pacman-5.0.1..c2f97580
   patch -p1 -i $srcdir/pacman-5.0.1-roundup.patch
}

build() {
  cd "$srcdir"/$pkgname-$pkgver

  ./configure --prefix=/usr --sysconfdir=/etc \
    --localstatedir=/var --enable-doc \
    --with-scriptlet-shell=/usr/bin/bash \
    --with-ldconfig=/usr/bin/ldconfig
  make V=1
  make -C contrib
}

check() {
  make -C "$pkgname-$pkgver" check
}

package() {
  cd "$srcdir"/$pkgname-$pkgver
  make DESTDIR="$pkgdir" install
  make DESTDIR="$pkgdir" -C contrib install

  # install Chakra specific stuff
  install -dm755 "$pkgdir"/etc
  install -m644 "$srcdir"/pacman.conf "$pkgdir"/etc/pacman.conf

  mycarch="x86_64"
  mychost="x86_64-pc-linux-gnu"
  myflags="-march=x86-64"

  install -m644 "$srcdir"/makepkg.conf "$pkgdir"/etc/
  # set things correctly in the default conf file
  sed -i "$pkgdir"/etc/makepkg.conf \
    -e "s|@CARCH[@]|$mycarch|g" \
    -e "s|@CHOST[@]|$mychost|g" \
    -e "s|@CARCHFLAGS[@]|$myflags|g"

  # put bash_completion in the right location
  install -dm755 ${pkgdir}/usr/share/bash-completion/completions
  mv ${pkgdir}/etc/bash_completion.d/pacman \
    ${pkgdir}/usr/share/bash-completion/completions
  rmdir ${pkgdir}/etc/bash_completion.d

  for f in makepkg pacman-key; do
    ln -s pacman "$pkgdir"/usr/share/bash-completion/completions/$f
  done

  install -Dm644 contrib/PKGBUILD.vim "$pkgdir"/usr/share/vim/vimfiles/syntax/PKGBUILD.vim
}
