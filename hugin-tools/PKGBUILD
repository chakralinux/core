pkgname=hugin-tools
_pkgname=hugin
pkgver=2017.0.0
pkgrel=3
pkgdesc="Hugin libs and tools to be used by Kipi-plugins"
arch=('x86_64')
url="http://hugin.sourceforge.net/"
license=('LGPL')
depends=('boost-libs' 'enblend-enfuse' 'exiv2>=0.25' 'fftw' 'perl-exiftool' 'openexr' 'libpano13>=2.9.19'
         'sqlite3' 'lapack' 'glew' 'desktop-file-utils' 'icu' 'libxt' 'libpng' 'libxrandr' 'openexr')
makedepends=('zip' 'cmake' 'boost' 'mesa' 'tclap' 'swig')
source=("http://downloads.sourceforge.net/${_pkgname}/${_pkgname}-${pkgver}.tar.bz2")
sha1sums=('4ba7f5bf5afdadbea9cf10d15aa14e644ef94ed3')

prepare() {
  cd "${srcdir}/${_pkgname}-${pkgver}"
}

build() {
  cd "${srcdir}"

  mkdir -p build 
  cd build

  # this makes possible to compile all the hugin tools without GUis
  sed -i -e "s~FIND_PACKAGE(wxWidgets REQUIRED)~~g" ${srcdir}/${_pkgname}-${pkgver}/CMakeLists.txt
  mkdir -p ${srcdir}/${_pkgname}-${pkgver}/src/hugin_base/PT

  cmake "${srcdir}/${_pkgname}-${pkgver}" \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_EXE_LINKER_FLAGS="-lpthread" \
    -DENABLE_LAPACK=yes \
    -DBUILD_HSI=no
  make
}

package() {
  cd "${srcdir}/build"
  make DESTDIR="${pkgdir}" install

  # Remove frontend files
  rm -rf -v "${pkgdir}"/usr/share/applications
  rm -rf -v "${pkgdir}"/usr/share/man
  rm -rf -v "${pkgdir}"/usr/share/locale
}
